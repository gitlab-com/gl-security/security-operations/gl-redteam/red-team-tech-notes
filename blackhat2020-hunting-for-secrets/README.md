## Black Hat USA 2020 - Arsenal - Token-Hunter and gitrob:  Hunting for Secrets

Contains video, slides, demos, and other artifacts from Greg Johnsons' talk on hunting for secrets with new tool, [Token-Hunter](https://gitlab.com/gitlab-com/gl-security/gl-redteam/token-hunter), and a updated version of [gitrob](https://gitlab.com/gitlab-com/gl-security/gl-redteam/gitrob) with many new features.  Demos are playable with the [asciinema tool](http://asciinema.org/).

![Video Demo (click lower-right to expand)](BH-2020-Arsenal-Greg-Johnson-SecretsHunting.mp4)

- [Video Download (mp4)](./BH-2020-Arsenal-Greg-Johnson-SecretsHunting.mp4)
- [Talk Slides Download](./BH-USA2020-Arsenal-HuntingForSecrets.key)
- [Talk Outline and Notes](./bh-2020-talk-outline.txt)
- [Gitrob Demo](./demos/gitrob-demo.asciinema)
- [Token-Hunter Demo](./demos/token-hunter-demo.asciinema)